<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome.html">
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="fname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>
        
        <label for="gender">Gender:</label><br>
        <input type="radio" id="value">
        <label for="male">Male</label><br>
        <input type="radio" id="value">
        <label for="female">Female</label><br>
        <input type="radio" id="value">
        <label for="other">Other</label>
        <br>
        <label for="nationality">Nationality:</label><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="British">British</option>
            <option value="Chinese">Chinese</option>
            <option value="Malayan">Malayan</option>
        </select>
        <br>
        <label for="language">Language Spoken:</label><br>
        <input type="checkbox">Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <label for="bio">Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>